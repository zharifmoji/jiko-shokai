<div align="center">

<img src="https://gitlab.com/viknes10/idp-weekly-logbook/-/raw/main/Upm_Logo.png" width=350 align=middle>

</div>

<div align="center">

### Department of Aerospace Engineering 
### EAS4947-1: PROJEK REKA BENTUK AEROANGKASA 
### (AEROSPACE DESIGN PROJECT)

</div>


## Front cover 

| Heading | Information |
| ------ | ----------- |
| Name | Muhammad Zulzharif Bin Zulkifli |
| Matric Number   | 198549 |
| Year of Study  | Year 4 |
| Subsystem Group | IDP Design and Structure |
| Team   | 6 |
| Lecturer / Supervisor   | DR. AHMAD SALAHUDDIN BIN MOHD HARITHUDDIN |

## Content of the Log

| Log Book Content | 
| ------ | 
| 1. Agenda   |
| 2. Goals   |
| 3. Method and Justification  |
| 4. Impact of the decision taken into   |
| 5. Next progress or step   |



<div align="center">

## **Page 1**

</div>

<div align="center">

<img src="https://gitlab.com/viknes10/idp-logbook-1/-/raw/main/R__1_.png" width=200 align=middle>

</div>

## Log Book 01 

| No. | Content | Descriptions |
| ------ | ------ | ------ |
| 1 | Agenda|Create a gantt chart for the flight system intergration subsystem based on the task given by Fikri.|
| 2 | Goals |To have a clear and align progress with other subsystems.|
| 3 | Method and Justification |By observing other subsytems gantt chart, we align the time and the task that were given in order for project's progress is moving smoothly with other subsystems.  |
| 4 | Impact of the decision taken into  |Since our group is the group that need to intergrate the entire project, our gantt chart is basically the guide for other to recognize the overall flow of the project, so that other subsystems can follow up with other subsystems. |
| 5 | Next progress or step |Presenting the gantt chart to everyone, update the logbook, check the work done for the previous task on gitlab. |




<div align="center">

## **Page 2**

</div>

## Log book 02

| No. | Content | Descriptions |
| ------ | ------ | ------ |
| 1 | Agenda|Check the current progress of the project at lab and presenting the gantt chart.|
| 2 | Goals |To align goals and project progress, and review the task that need to be prioritize.|
| 3 | Method and Justification |Our group presenter, Syahirah aka YB has already presented the gantt chart of our subsystems to other subsystems for they to understand the job scope of our group and what will our group do along the 14 weeks. By explaining the tasks flow and the duration of the tasks going have implant the target set which is align with the milestone of the project. Since our group is insential to other subsystems group, this will help other subsystems group to understand the flow of the project better. As for the logbook, it is just to recorded the track of our project. |
| 4 | Impact of the decision taken into  |As mentioned above, by presenting the gantt chart of our subsytems group, other subsystems group to understand the flow of the project better.|
| 5 | Next progress or step |Execute the task given for the subsystems and monitor the work done by other subsystems group. |

<div align="center">

## **Page 3**

</div>


## Log book 03

| No. | Content | Descriptions |
| ------ | ------ | ------ |
| 1 | Agenda| Monitor the progress of the project and other subsystems, update the progress and help the other subsystems if needed. Find the CG of the airship without the payload.|
| 2 | Goals |To secure that the progress is align with the gantt charts. To obtained the CG of the airship without payload.|
| 3 | Method and Justification |I asked my small group for their subsystems current progress based on the task given and their gantt chart. Check the current progress whether it is align or not with the gantt chart. Some of our group members helps propulsion team to conduct a little test to obtain the thrust of the airship to move foward.|
| 4 | Impact of the decision taken into  |By checking the status of each subsystem, we able to checked whether the status is aligned or not with the proposed gantt charts in order to have a clear view of the project.|
| 5 | Next progress or step |Monitor the progress of other subsystems group. |



<div align="center">

## **Page 4**

</div>
   

## Log book 04

| No. | Content | Descriptions |
| ------ | ------ | ------ |
| 1 | Agenda| Monitor other subsytems group progress and check whether they still on track or not |
| 2 | Goals |To align the progress of the work as supposed in the gantt chart for a smooth project progress. |
| 3 | Method and Justification |Check the current progress of each subsytems and compare to the proposed gantt chart. By doing this, we able to track the progress carefully and make sure whether it align or not with the progress of the project.|
| 4 | Impact of the decision taken into  |Having all of the team on the track will greatly boost the progress of the project. |
| 5 | Next progress or step |Find the CG of the aircraft. Perform the integration for small HAU. Monitor the work of other subsystems.|

<div align="center">

## **Page 5**

</div>


## Log book 05

| No. | Content | Descriptions |
| ------ | ------ | ------ |
| 1 | Agenda| Present the table that Dr. Salah ask to gather last week. Monitor other subsytems progress and the equipment tracker. |
| 2 | Goals |To ensure that the progress of each group is align with the gantt chart proposed and to present the task given. |
| 3 | Method and Justification |Ask the available paramater from the simulation, propulsion and design team as they obatained or measured the parameters due to their task. |
| 4 | Impact of the decision taken into  |Hence, we able to estimate the theoretical performance of the HUA. |
| 5 | Next progress or step |Meeting with Dr Salah for new task.|



<div align="center">

## **Page 6**

</div>

## Log book 06

| No. | Content | Descriptions |
| ------ | ------ | ------ |
| 1 | Agenda| Weekly progress of each subsystems. |
| 2 | Goals |New task discussion with subsystem group |
| 3 | Method and Justification |Discussion among the group after meeting with Dr Salah about the template of report. |
| 4 | Impact of the decision taken into  |Our susbsystem has been asign for the report development which will provide the documantation of the porject. |
| 5 | Next progress or step |Develop the report. |


<div align="center">

## **Page 7**

</div>
